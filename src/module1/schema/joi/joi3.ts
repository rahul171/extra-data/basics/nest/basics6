const Joi = require('@hapi/joi');

export const schema = Joi.object({
  p1: Joi.string().required(),
  p2: Joi.number(),
});
