import { ObjectSchema } from '@hapi/joi';
const Joi = require('@hapi/joi');

export const schema: ObjectSchema = Joi.object({
  p1: Joi.string(),
  p2: Joi.number(),
  p3: Joi.boolean(),
});
