import { IsBoolean, IsNumber, IsString } from 'class-validator';

export class Dto1 {
  @IsNumber()
  p1: number;

  @IsString()
  p2: string;

  @IsBoolean()
  p3: boolean;
}
