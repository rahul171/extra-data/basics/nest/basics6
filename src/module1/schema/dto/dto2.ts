import { IsBoolean, IsNumber, IsString } from 'class-validator';

export class Dto2 {
  @IsNumber()
  p11: number;

  @IsString()
  p22: string;

  @IsBoolean()
  p33: boolean;
}
