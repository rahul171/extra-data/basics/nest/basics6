import {
  IsBoolean,
  IsNumber,
  IsNumberString,
  IsOptional,
  IsString,
} from 'class-validator';

export class Dto3 {
  @IsString()
  p1: number;

  @IsNumberString()
  p2: string;

  @IsNumber()
  @IsOptional()
  p3: number;
}
