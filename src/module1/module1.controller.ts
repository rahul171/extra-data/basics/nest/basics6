import {
  Body,
  Controller,
  DefaultValuePipe,
  Get,
  HttpStatus,
  Param,
  ParseIntPipe,
  Post,
  UsePipes,
} from '@nestjs/common';
import { Module1Service } from './module1.service';
import { CustomPipe } from '../common/pipes/custom.pipe';
import { Dto1 } from './schema/dto/dto1';
import { ValidationPipe } from '../common/pipes/validation.pipe';
import { JoiValidatePipe } from '../common/pipes/joi-validate.pipe';
import { schema as schema1 } from './schema/joi/joi1';
import { schema as schema2 } from './schema/joi/joi2';
import { schema as schema3 } from './schema/joi/joi3';
import { schema as schema4 } from './schema/joi/joi4';
import { Dto2 } from './schema/dto/dto2';
import { Dto3 } from './schema/dto/dto3';
import { ControllerPipe } from '../common/pipes/controller.pipe';

@Controller('module1')
@UsePipes(ControllerPipe)
export class Module1Controller {
  constructor(private module1Service: Module1Service) {}

  @Get()
  getMessage(): string {
    return this.module1Service.getMessage();
  }

  @Get('user/:id')
  getUser(@Param('id') id1, @Param('id', ParseIntPipe) id2: number) {
    return {
      id1: {
        type: typeof id1,
        value: id1,
      },
      id2: {
        type: typeof id2,
        value: id2,
      },
    };
  }

  @Get('user2/:id')
  getUser2(
    @Param(
      'id',
      new ParseIntPipe({ errorHttpStatusCode: HttpStatus.FORBIDDEN }),
    )
    id: number,
  ) {
    return {
      id: {
        type: typeof id,
        value: id,
      },
    };
  }

  @Get('user3/:id')
  getUser3(
    @Param('id') id1,
    @Param('id', new CustomPipe({ something: 'someone' })) id2: number,
  ) {
    return this.module1Service.getResponse(id1, id2);
  }

  // @Get('user4/:p1/:p2/:p3')

  @Post('user4')
  user4(@Body(new ValidationPipe()) body: Dto1) {
    return this.module1Service.getParamsResponse({ body });
  }

  @Post('user5')
  user5(@Body() body: Dto2) {
    return this.module1Service.getParamsResponse({ body });
  }

  @Post('user6')
  @UsePipes(new JoiValidatePipe(schema1))
  user6(@Body() body) {
    return this.module1Service.getParamsResponse({ body });
  }

  @Get('user7/:p1/:p2?')
  @UsePipes(new JoiValidatePipe(schema2))
  user7(@Param() params) {
    return this.module1Service.getParamsResponse({ params });
  }

  @Post('user8/:p1/:p2?')
  @UsePipes(new JoiValidatePipe(schema3))
  // validation starts from the last parameter.
  // last parameter gets validated first, then the second last and so on.

  // if the first validation fails, seconds validation will still be performed,
  // but the route handler won't get called.
  // think of it like calling res.send() in express route handler.
  // you will still execute the code after res.send() method, if there is a next()
  // then you will also execute that next() route handler, its just that you
  // can't send the response again to the same request, but you are able
  // to continue execution of the code.
  user8(@Body() body, @Param() params, @Param('p1') paramP1: number) {
    return this.module1Service.getParamsResponse({ params, body });
  }

  @Post('user9/:p1/:p2?')
  user9(@Param(new JoiValidatePipe(schema4)) params, @Body() body) {
    return this.module1Service.getParamsResponse({ params, body });
  }

  @Post('user10/:p1/:p2?')
  @UsePipes(ValidationPipe)
  user10(@Param() params: Dto3, @Body() body: Dto3) {
    return this.module1Service.getParamsResponse({ params, body });
  }

  // the difference between joi schema validation and class-validator methods
  // is, class-validator pipe can be registered globally and it will take
  // schema from the every route handler, whereas joi schema validation pipe
  // can be registered globally but we also have to provide a schema at that time,
  // hence it will use the same schema for every route handler.
}
