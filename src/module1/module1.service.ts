import { Injectable } from '@nestjs/common';

@Injectable()
export class Module1Service {
  getMessage(): string {
    return 'welcome to the module1 service';
  }

  getResponse(...args) {
    const response = {};

    args.forEach((item, index) => {
      response[`param-${index + 1}`] = {
        type: typeof item,
        value: item,
      };
    });

    return response;
  }

  getParamsResponse(arg) {
    console.log('getParamsResponse =>', arg);
    return arg;
  }
}
