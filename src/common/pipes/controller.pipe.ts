import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';

@Injectable()
export class ControllerPipe implements PipeTransform {
  transform(value: any, { metatype }: ArgumentMetadata) {
    console.log('----CONTROLLER PIPE----');
    console.log('value =>', value);
    console.log(`ValidationPipe => transform() => metatype (${metatype})`);
    console.log('----END CONTROLLER PIPE----');
    return value;
  }
}
