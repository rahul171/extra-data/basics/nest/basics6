import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  PipeTransform,
} from '@nestjs/common';
import { validate } from 'class-validator';
import { plainToClass } from 'class-transformer';

@Injectable()
export class ValidationPipe implements PipeTransform<any> {
  async transform(value: any, { metatype }: ArgumentMetadata) {
    console.log('----VALIDATION PIPE----');
    console.log('value =>', value);
    console.log(`ValidationPipe => transform() => metatype (${metatype})`);
    if (!metatype || !this.toValidate(metatype)) {
      return value;
    }

    const object = plainToClass(metatype, value);
    const errors = await validate(object);

    console.log('-- after await --');
    console.log('value =>', value);
    console.log('error =>', errors);

    if (errors.length > 0) {
      throw new BadRequestException(errors);
    }

    console.log('----END VALIDATION PIPE----');
    return value;
  }

  private toValidate(metatype: Function): boolean {
    const types: Function[] = [String, Number, Boolean, Array, Object];
    return !types.includes(metatype);
  }
}
