import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  PipeTransform,
} from '@nestjs/common';
import { ObjectSchema } from '@hapi/joi';

@Injectable()
export class JoiValidatePipe implements PipeTransform {
  constructor(private schema: ObjectSchema) {}

  transform(value: any, { metatype }: ArgumentMetadata) {
    console.log('----JOI PIPE----');
    console.log('value =>', value);
    console.log(`JoiValidatePipe => transform() => metatype (${metatype})`);

    const { value: outVal, error } = this.schema.validate(value);

    if (error) {
      console.log('error =>', error);
      throw new BadRequestException(error);
    }

    console.log('----END JOI PIPE----');
    return value;
  }
}
