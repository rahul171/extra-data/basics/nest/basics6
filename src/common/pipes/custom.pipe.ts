import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common';

@Injectable()
export class CustomPipe implements PipeTransform {
  constructor(...args) {
    console.log('CustomPipe => constructor');
    console.log(args);
  }

  transform(value: any, metadata: ArgumentMetadata) {
    console.log('CustomPipe => transform()');
    console.log(value);
    console.log(typeof value);
    console.log(metadata);
    console.log(metadata.metatype);
    const value1: any = Number(value);
    console.log(value1 instanceof metadata.metatype);
    console.log(new metadata.metatype(11) == 11);
    return value;
  }
}
