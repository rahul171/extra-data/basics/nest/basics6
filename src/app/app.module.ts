import { Module } from '@nestjs/common';
import { AppService } from './app.service';
import { AppController } from './app.controller';
import { Module1Module } from '../module1/module1.module';

@Module({
  imports: [Module1Module],
  providers: [AppService],
  controllers: [AppController],
})
export class AppModule {}
